<div class="product_item">
<a href="<?php echo BASE_URL;?>product/open/<?php echo $id; ?>">
    <div class="product_tags">
        <?php if ($sale == 1): ?>
        <div class="product_tag product_tag_red"><?php $this->lang->get("SALE");?></div>
        <?php endif; ?>

        <?php if ($bestseller == 1): ?>
        <div class="product_tag product_tag_green"><?php $this->lang->get("BESTSELLER");?></div>
        <?php endif; ?>

        <?php if ($new_product == 1): ?>
        <div class="product_tag product_tag_blue"><?php $this->lang->get("NEW");?></div>
        <?php endif; ?>
    </div>
    <div class="product_image img-responsive">
        <img src="<?php echo BASE_URL;?>media/products/<?php echo $images[0]; ?>"
             width="100%">
    </div>
    <div class="product_name">
        <?php echo $name; ?>
    </div>
    <div class="product_brand">
        <?php echo $brandName; ?>
    </div>
    <div class="product_price">
    <?php if ($price_from != 0): ?>
        <?php echo 'R$ '.number_format($price_from, 2, ',', '.'); ?>
    <?php endif; ?>
    </div>

    <div class="product_price_sale">
        <?php echo 'R$ '.number_format($price, 2, ',', '.'); ?>
    </div>
    <div style="clear: both"></div>
</a>
</div>