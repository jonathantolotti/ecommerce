<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Projeto E-Commerce 2018</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/jquery-ui.min.css"
              type="text/css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/jquery-ui.structure.min.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/jquery-ui.theme.min.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style.css" type="text/css" />
        <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	</head>
	<body>
    <div class="container-fluid">
        <div class="alert alert-info" role="alert" style=" font-family: 'Calibri Light'; padding: 5px; text-align: center; margin-top: 5px; margin-bottom: 5px;">
            Este projeto foi criado para fins acadêmicos por <a href="https://www.jonathantolotti.com.br" class="alert-link" target="_blank">Jonathan Tolotti</a>, todas as informações são fictícias. Ao navegar neste site, você concorda que todo seu acesso está sendo registrado.
        </div>
        <?php if(ENVIRONMENT == 'development'): ?>
            <div class="alert alert-danger" role="alert" style=" font-family: 'Calibri Light'; padding: 5px; text-align: center; margin-top: 5px; margin-bottom: 5px;" id="ambiente">
                <span class="ambiente"><b>ATENÇÃO!</b> Rodando em ambiente de desenvolvimento, <b>ALTERAR PARA PRODUÇÃO ANTES DO DEPLOY</b>.</span>
            </div>
        <?php endif; ?>
    </div>
		<nav class="navbar topnav">
			<div class="container">
				<ul class="nav navbar-nav">
					<li class="active">
                        <a href="<?php echo BASE_URL; ?>"><?php $this->lang->get("HOME");?></a></li>
					<li><a href="<?php echo BASE_URL; ?>contact"><?php $this->lang->get("CONTACT");
					?></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <?php $this->lang->get("LANGUAGE");?>
						<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo BASE_URL;?>lang/set/en">English</a></li>
							<li><a href="<?php echo BASE_URL;?>lang/set/pt-br">Português</a></li>
						</ul>
					</li>
                    <?php if (empty($_SESSION['userID'])): ?>
					<li>
                        <a href="<?php echo BASE_URL; ?>users/validate"><?php $this->lang->get("LOGIN");?></a>
                    </li>
                    <?php endif; ?>
                    <?php if (!empty($_SESSION['userID'])): ?>
                    <li>
                        <a href="<?php echo BASE_URL;?>users/myorders">Meus pedidos</a>
                    </li>
                    <li>
                        <a href="<?php echo BASE_URL;?>users/logout">Sair</a>
                    </li>
                    <?php endif; ?>
				</ul>
			</div>
		</nav>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-sm-2 logo">
						<a href="<?php echo BASE_URL; ?>"><img src="<?php echo BASE_URL;
						?>assets/images/logo.png"/></a>
					</div>
					<div class="col-sm-7">
						<div class="head_help">(11) 9999-9999</div>
						<div class="head_email">contato@<span>loja2.com.br</span></div>
						
						<div class="search_area">
							<form method="GET" action="<?php echo BASE_URL;?>search">
								<input type="text" name="product"
                                       value="<?php echo (!empty($viewData['searchTerm']))
                                           ?$viewData['searchTerm']:'';?>" required
                                       placeholder="<?php $this->lang->get("SEARCHITEM");?>" />
								<select name="category">
                                    <option value="">
                                        <?php $this->lang->get("ALLCATEGORIES");?>
                                    </option>

                                    <?php foreach ($viewData['categories'] as $category): ?>
                                    <option <?php echo((!empty($viewData['category']))&&
                                        ($viewData['category']==$category['id']))
                                        ?'selected="selected"':''; ?>
                                            value="<?php echo
                                    $category['id']; ?>">
                                        <?php echo $category['name']; ?>
                                    </option>

                                        <?php
                                        if (count($category['subs']) > 0) {
                                            $this->loadView('search_subcategory', [
                                                'subs' => $category['subs'],
                                                'level' => 1,
                                                'category' => $viewData['category']
                                            ]);
                                        }
                                        ?>
                                    <?php endforeach; ?>



								</select>
								<input type="submit" value="" />
						    </form>
						</div>
					</div>
					<div class="col-sm-3">
						<a href="<?php echo BASE_URL; ?>cart">
							<div class="cartarea">
								<div class="carticon">
									<div class="cartqt">
                                        <?php if (isset($viewData['cart_qt'])): ?>
                                        <?php echo $viewData['cart_qt']; ?>
                                        <?php else: ?>
                                        0
                                        <?php endif; ?>
                                        </div>
								</div>
								<div class="carttotal">
                                    <?php $this->lang->get("CART");?><br/>
									<span><?php echo 'R$ '.number_format($viewData['cart_price'],
                                                2, ",", "."); ?></span>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</header>
        <?php if (isset($viewData['sidebar'])): ?>
		<div class="categoryarea">
			<nav class="navbar">
				<div class="container">
					<ul class="nav navbar-nav">
						<li class="dropdown">
					        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <?php $this->lang->get("SELECTCATEGORY");?>
					        <span class="caret"></span></a>
					        <ul class="dropdown-menu">
					          <?php foreach ($viewData['categories'] as $category): ?>
                                <li>
                                    <a href="<?php echo BASE_URL.'categories/enter/'.$category['id'];?>">
                                        <?php echo $category['name']; ?>
                                    </a>
                                </li>
                              <?php
                                if (count($category['subs']) > 0) {
                                    $this->loadView('menu_subcategory', [
                                            'subs' => $category['subs'],
                                            'level' => 1
                                    ]);
                                }
                              ?>
                              <?php endforeach; ?>
					        </ul>
					      </li>
                        <?php if(isset($viewData['categoryFilter'])): ?>
                            <?php foreach($viewData['categoryFilter'] as $cf): ?>
                                <li>
                                    <a href="<?php echo BASE_URL; ?>categories/enter/<?php echo $cf['id']; ?>">
                                        <?php echo $cf['name']; ?></a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
					</ul>
				</div>
			</nav>
		</div>
        <?php endif; ?>
		<section>
			<div class="container">
				<div class="row">
                 <?php if (isset($viewData['sidebar'])): ?>
				  <div class="col-sm-3">
				  	<!-- INSERE A VIEW DE FILTROS -->
				  	<?php $this->loadView('sidebar', ['viewData' => $viewData]); ?>
				  </div>
				  <div class="col-sm-9">
                      <?php $this->loadViewInTemplate($viewName, $viewData); ?>
                   </div>
				  <?php else: ?>
                     <div class="col-sm-12">
                      <?php $this->loadViewInTemplate($viewName, $viewData); ?>
                   </div>
				  <?php endif; ?>
				</div>
	    	</div>
	    </section>
	    <footer>
	    	<div class="container">
	    		<div class="row">
				  <div class="col-sm-4">
				  	<div class="widget">
			  			<h1><?php $this->lang->get("FEATUREDPRODUCTS");?></h1>
			  			<div class="widget_body">
			  				<?php $this->loadView('widget_item', ['list' =>
			  				$viewData['widget_featured2']]); ?>
			  			</div>
			  		</div>
				  </div>
				  <div class="col-sm-4">
				  	<div class="widget">
			  			<h1><?php $this->lang->get("ONSALEPRODUCTS");?></h1>
			  			<div class="widget_body">
                            <?php $this->loadView('widget_item', ['list' => $viewData['widget_sale']]); ?>
			  			</div>
			  		</div>
				  </div>
				  <div class="col-sm-4">
				  	<div class="widget">
			  			<h1><?php $this->lang->get("TOPRATEDPRODUCTS");?></h1>
			  			<div class="widget_body">
			  				<?php $this->loadView('widget_item', ['list' =>
			  				$viewData['widget_toprated']]); ?>
			  			</div>
			  		</div>
				  </div>
				</div>
	    	</div>
	    	<div class="subarea">
	    		<div class="container">
	    			<div class="row">
						<div class="col-xs-12 col-sm-8 col-sm-offset-2 no-padding">
                            <form action="https://jonathantolotti.us19.list-manage.com/subscribe/post?u=d4e6d42f5cbbd1b8d5130fee1&amp;id=548d782212" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <input type="email" placeholder="
                                <?php $this->lang->get("SUBSCRIBETEXT");?>" name="EMAIL"
                                       class="required email subemail"
                                       id="mce-EMAIL">
                                <input type="hidden" name="b_d4e6d42f5cbbd1b8d5130fee1_548d782212"
                                       tabindex="-1" value="">
                                <input type="submit" value="<?php $this->lang->get("SUBSCRIBEBUTTON");?>" name="subscribe" id="mc-embedded-subscribe" class="button" />
                            </form>
						</div>
					</div>
	    		</div>
	    	</div>
	    	<div class="links">
	    		<div class="container">
	    			<div class="row">
						<div class="col-sm-4">
							<a href="<?php echo BASE_URL; ?>"><img width="150" src="<?php echo BASE_URL; ?>assets/images/logo.png" /></a><br/><br/>
							<strong>Slogan da Loja Virtual</strong><br/><br/>
							Endereço da Loja Virtual
						</div>
						<div class="col-sm-8 linkgroups">
							<div class="row">
								<div class="col-sm-4">
									<h3><?php $this->lang->get("CATEGORIES");?></h3>
									<ul>
										<li><a href="#">Categoria X</a></li>
										<li><a href="#">Categoria X</a></li>
										<li><a href="#">Categoria X</a></li>
										<li><a href="#">Categoria X</a></li>
										<li><a href="#">Categoria X</a></li>
										<li><a href="#">Categoria X</a></li>
									</ul>
								</div>
								<div class="col-sm-4">
									<h3><?php $this->lang->get("INFORMATION");?></h3>
									<ul>
										<li><a href="#">Menu 1</a></li>
										<li><a href="#">Menu 2</a></li>
										<li><a href="#">Menu 3</a></li>
										<li><a href="#">Menu 4</a></li>
										<li><a href="#">Menu 5</a></li>
										<li><a href="#">Menu 6</a></li>
									</ul>
								</div>
								<div class="col-sm-4">
									<h3><?php $this->lang->get("INFORMATION");?></h3>
									<ul>
										<li><a href="#">Menu 1</a></li>
										<li><a href="#">Menu 2</a></li>
										<li><a href="#">Menu 3</a></li>
										<li><a href="#">Menu 4</a></li>
										<li><a href="#">Menu 5</a></li>
										<li><a href="#">Menu 6</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
	    		</div>
	    	</div>
	    	<div class="copyright">
	    		<div class="container">
	    			<div class="row">
						<div class="col-sm-6">© <span>Loja 2.0</span> - Todos os direitos reservados.</div>
						<div class="col-sm-6">
							<div class="payments">
								<img src="<?php echo BASE_URL; ?>assets/images/visa.png" />
								<img src="<?php echo BASE_URL; ?>assets/images/visa.png" />
								<img src="<?php echo BASE_URL; ?>assets/images/visa.png" />
								<img src="<?php echo BASE_URL; ?>assets/images/visa.png" />
							</div>
						</div>
					</div>
	    		</div>
	    	</div>
	    </footer>
		<script type="text/javascript">
                var BASE_URL = '<?php echo BASE_URL; ?>';
                <?php if (isset($viewData['filters'])): ?>
                var maxslider = <?php echo $viewData['filters']['maxslider']; ?>;
                var minslider = <?php echo $viewData['filters']['minslider']; ?>;
                <?php endif; ?>
        </script>

		<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-ui.min
		.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script.js"></script>
	</body>
</html>