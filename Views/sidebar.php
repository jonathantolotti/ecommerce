<aside>
    <h1><?php $this->lang->get("FILTER");?></h1>
    <div class="filterarea">
        <form method="get">
            <?php if (!empty($viewData['searchTerm'])): ?>
                <input type="hidden" name="product" value="<?php echo
                $viewData['searchTerm']; ?>">
                <input type="hidden" name="category" value="<?php echo
                $viewData['category'];
                ?>">
            <?php endif; ?>
            <div class="filterbox">
                <div class="filtertitle">
                                    <?php echo $this->lang->get("BRANDS"); ?>
                                </div>
                <div class="filtercontent">
                    <?php foreach ($viewData['filters']['brands'] as $brands): ?>
                        <div class="filteritem">
                            <input type="checkbox" <?php echo (isset($viewData['filtersSelected']['brand']) && in_array
                                ($brands['id'], $viewData['filtersSelected']['brand']))?
                                'checked="checked"':'' ?>
                                   name="filter[brand][]"
                                   value="<?php echo $brands['id'] ?>"
                                   id="brand<?php echo $brands['id'] ?>">
                            <label for="brand<?php echo $brands['id']; ?>">
                                <?php echo $brands['name']; ?>
                            </label>
                            <span style="float: right">(<?php echo $brands['count']; ?>)</span>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>

            <div class="filterbox">
                <div class="filtertitle">
                                    <?php echo $this->lang->get("PRICE"); ?>
                                </div>
                <div class="filtercontent">
                    <input type="hidden" name="filter[slider0]" id="slider0"
                           value="<?php echo $viewData['filters']['slider0']; ?>">
                    <input type="hidden" name="filter[slider1]" id="slider1"
                           value="<?php echo $viewData['filters']['slider1']; ?>">
                    <p>
                        <input type="text" id="amount" readonly style="border:0; font-weight:bold;">
                    </p>

                    <div id="slider-range"></div>
                </div>
            </div>

            <div class="filterbox">
                <div class="filtertitle">
                                    <?php echo $this->lang->get("RATING"); ?>
                                </div>
                <div class="filtercontent">
                    <div class="filteritem">
                        <input type="checkbox" <?php echo (isset($viewData['filtersSelected']['star']) && in_array
                            ('0', $viewData['filtersSelected']['star']))?
                            'checked="checked"':'' ?>
                               name="filter[star][]"
                               value="0" id="star0">
                        <label for="star0">
                            (<?php echo $this->lang->get("NOSTAR"); ?>)
                        </label>
                        <span style="float: right">(<?php echo
                            $viewData['filters']['stars']['0'];
                            ?>)</span>
                    </div><!-- item -->
                    <?php for($q=1; $q<=5; $q++): ?>
                        <div class="filteritem">
                            <input type="checkbox" <?php echo (isset($viewData['filtersSelected']['star']) && in_array
                                ($q, $viewData['filtersSelected']['star']))?
                                'checked="checked"':'' ?>
                                   name="filter[star][]"
                                   value="<?php echo $q; ?>" id="star<?php echo $q; ?>">
                            <label for="star<?php echo $q; ?>">
                                <?php $i = 1;
                                while($i <= $q): $i++; ?>
                                    <img src="<?php echo BASE_URL;
                                    ?>assets/images/star.png" width="15">
                                <?php endwhile; ?>
                            </label>
                            <span style="float: right">(<?php echo
                                $viewData['filters']['stars'][$q];
                                ?>)</span>
                        </div><!-- item -->
                    <?php endfor; ?>
                </div>
            </div>

            <div class="filterbox">
                <div class="filtertitle">
                                    <?php echo $this->lang->get("SALEFILTER"); ?>
                                </div>
                <div class="filtercontent">
                    <div class="filteritem">
                        <input type="checkbox" <?php echo (isset($viewData['filtersSelected']['sale']) &&                                                                                   $viewData['filtersSelected']['sale'] == '1')?
                            'checked="checked"':'' ?>
                               value="1"
                               name="filter[sale]" id="onsale">
                        <label for="onsale"><?php echo $this->lang->get("ONSALE");?></label>
                        <span style="float: right">(<?php echo $viewData['filters']['sale'];
                            ?>)</span>
                    </div>
                </div>
            </div>

            <div class="filterbox">
                <div class="filtertitle">
                                    <?php echo $this->lang->get("OPTIONS"); ?>
                                </div>
                <div class="filtercontent">
                    <?php foreach ($viewData['filters']['options'] as $option): ?>

                        <strong><?php echo $option['name']; ?></strong><br>
                        <?php foreach ($option['options'] as $op): ?>
                            <div class="filteritem">
                                <input type="checkbox" <?php echo (isset
                                    ($viewData['filtersSelected']['options']) &&
                                    in_array
                                    ($op['value'], $viewData['filtersSelected']['options']))?
                                    'checked="checked"':'' ?>
                                       name="filter[options][]"
                                       value="<?php echo $op['value'] ?>"
                                       id="option<?php echo $op['id'] ?>">
                                <label for="option<?php echo $op['id']; ?>">
                                    <?php echo $op['value']; ?>
                                </label>
                                <span style="float: right">(<?php echo $op['count'];
                                    ?>)</span>
                            </div>
                        <?php endforeach; ?>
                        <br>
                    <?php endforeach; ?>
                </div>
            </div>

        </form>
    </div>

    <div class="widget">
        <h1><?php $this->lang->get("FEATUREDPRODUCTS");?></h1>
        <div class="widget_body">
				  				<?php $this->loadView('widget_item', ['list' =>
			  				$viewData['widget_featured1']]); ?>
				  			</div>
    </div>
</aside>