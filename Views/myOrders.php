
<h1 class="text-center">Minhas compras:</h1>
<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th scope="col" style="text-align: center">Pedido</th>
            <th scope="col" style="text-align: center">Valor</th>
            <th scope="col" style="text-align: center">Status</th>
            <th scope="col" style="text-align: center"></th>
        </tr>
        </thead>
        <tbody>

            <?php foreach ($orders as $order): ?>
            <tr>
            <td style="text-align: center"><?php echo $order['id']; ?></td>
            <td style="text-align: center">
                <?php echo 'R$ '.number_format($order['total_amount'],2,",","."); ?></td>
            <td style="text-align: center"><?php echo $order['status_store']; ?></td>
                <td style="text-align: center">
                    <a href="<?php echo BASE_URL;?>orders/detailOrder/<?php echo
                    $order['id'];?>">Ver detalhes</a></td>
            </tr>
            <?php endforeach; ?>


        </tbody>
    </table>
</div>
