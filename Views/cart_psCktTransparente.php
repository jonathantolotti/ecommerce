<?php if (isset($_SESSION['userID'])): ?>
<div class="row">

        <div class="col-sm-4">
            <div class="form-group">
                <h3>Dados Pessoais</h3>

                <label>Nome:</label>
                <input type="text" name="name" class="form-control" value="Jonathan Tolotti" required/>

                <label>CPF:</label>
                <input type="text" name="cpf" class="form-control" value="02372079077" required/>

                <label>Telefone:</label>
                <input type="text" name="telefone" class="form-control" value="51987654432" required/>

                <label>E-mail:</label>
                <input type="email" name="email" class="form-control"
                       value="c77795441657780601711@sandbox.pagseguro.com.br" required/>

                <label>Senha:</label>
                <input type="password" name="password" class="form-control" value="85Dr7j88RL8JJdKc" required/>
            </div>
        </div>
        <div class="col-sm-4">
        <h3>Informações de Endereço</h3>
            <div class="form-group">

                <label>CEP:</label>
                <input type="text" name="cep" class="form-control" value="91920540" required/>

                <label>Rua:</label>
                <input type="text" name="rua" class="form-control" value="Rua Vigário Calixto" required/>

                <label>Número:</label>
                <input type="text" name="numero" class="form-control" value="1400" required/>

                <label>Complemento:</label>
                <input type="text" class="form-control" name="complemento" />

                <label>Bairro:</label>
                <input type="text" name="bairro" class="form-control" value="Catolé" required/>

                <label>Cidade:</label>
                <input type="text" name="cidade" class="form-control" value="Campina Grande" required/>

                <label>Estado:</label>
                <input type="text" name="estado" class="form-control" value="PB" required/>
            </div>
        </div>

        <div class="col-sm-4">
        <h3>Informações de Pagamento</h3>
            <div class="form-group">

                <label>Titular do cartão:</label>
                <input type="text" name="cartao_titular" class="form-control" value="Jonathan Tolotti" required/>

                <label>CPF do Titular do cartão:</label>
                <input type="text" name="cartao_cpf" class="form-control" value="02372079077"
                       required/>

                <label>Número do cartão:</label>
                <input type="text" class="form-control" name="cartao_numero" value="" maxlength="16" required/>

                <label>Código de Segurança:</label>
                <input type="text" name="cartao_cvv" class="form-control"  required/>

                <label>Validade:</label>
                <select class="form-control" name="cartao_mes" required>
                    <?php for($q=1;$q<=12;$q++): ?>
                        <option><?php echo ($q<10)?'0'.$q:$q; ?></option>
                    <?php endfor; ?>
                </select>
                <select class="form-control" name="cartao_ano" required>
                    <?php $ano = intval(date('Y')); ?>
                    <?php for($q=$ano;$q<=($ano+20);$q++): ?>
                        <option><?php echo $q; ?></option>
                    <?php endfor; ?>
                </select>

                <label>Parcelas:</label><br/>
                <select class="form-control" name="parc" required></select>

                <input type="hidden" name="total" value="<?php echo $total;?>" />
            </div>

            <button class="button efetuarCompra btn btn-success btn-lg btn-block">Efetuar compra</button>

        </div>

        </div>
<?php else: ?>
<?php $this->loadView('loginUser', []); ?>
<?php endif; ?>
<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com
.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>assets/js/chktransp.js"></script>
<script type="text/javascript">
    PagSeguroDirectPayment.setSessionId("<?php echo $sessionCode; ?>");
</script>