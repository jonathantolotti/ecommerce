<!-- vitrine de produtos -->
<div class="row">
<?php $count = 0; ?>
<?php foreach ($productList as $productItem):?>
    <div class="col-sm-4">
        <?php $this->loadView('productItem', $productItem); ?>
    </div>
    <?php
    if ($count >= 2) {
        $count = 0;
        echo '</div><div class="row">';
    } else {
        $count++;
    }
    ?>
<?php endforeach; ?>
</div>


<div class="paginationArea">
    <?php for($q=1;$q<=$numberPages;$q++): ?>
        <div class="paginationItem <?php echo ($currentPage==$q)?'pag_active':''; ?>"><a href="<?php echo BASE_URL; ?>?<?php
            $pag_array = $_GET;
            $pag_array['p'] = $q;
            echo http_build_query($pag_array);
            ?>"><?php echo $q; ?></a></div>
    <?php endfor; ?>
</div>