<?php if (!empty($emptyCart)): ?>
<h1 style="text-align: center;">Seu carrinho de compras está vazio!</h1>
<?php endif; ?>


<table border="0" width="100%" style="text-align: center">
    <tr>
        <th width="100" style="text-align: center">Imagem</th>
        <th style="text-align: center">Nome</th>
        <th style="text-align: center">Quantidade</th>
        <th style="text-align: center">Preço</th>
        <td width="20"></td>
    </tr>
    <?php $subtotal = 0; ?>
    <?php foreach ($list as $item): ?>
    <?php $subtotal += (floatval($item['price'])* intval($item['qt'])) ?>
        <tr>
            <td><img src="<?php echo BASE_URL;?>media/products/<?php echo $item['image']; ?>"
            width="80"></td>
            <td><?php echo $item['name']; ?></td>
            <td><?php echo $item['qt']; ?></td>
            <td><?php echo 'R$ '.number_format($item['price'], 2, ',', '.'); ?></td>
            <td>
                <a href="<?php echo BASE_URL;?>cart/del/<?php echo $item['id']; ?>">
                    <img src="<?php echo BASE_URL;?>assets/images/trash.png"
                                width="20" height="20">
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="3" align="right">Subtotal: </td>
        <td><b><?php echo 'R$ '.number_format($subtotal, 2, ',', '.'); ?></b></td>
    </tr>

    <tr>
        <td colspan="3" align="right">Frete: </td>
        <td>
            <?php if (isset($shipping['price'])): ?>
                <b><?php echo 'R$ '.$shipping['price']; ?> (Prazo: <?php echo $shipping['date']; ?>
                    dias úteis)</b>
            <?php else: ?>
                <span><b>R$ 0,00</b></span>
            <?php endif; ?>
        </td>
    </tr>

    <tr>
        <td colspan="3" align="right">Total: </td>
        <td><b><?php
                if (isset($shipping['price'])) {
                    $frete = floatval(str_replace(',','.',$shipping['price']));
                } else {
                    $frete = 0;
                }
                $total = $subtotal + $frete;
                echo 'R$ '.number_format($total, 2, ',', '.');
                ?></b></td>
    </tr>
</table>
<hr>
<form method="post" style="float: left;">
    <label>Informe seu CEP:</label>
    <input type="number" name="cep" maxlength="8">
    <input type="submit" value="Calcular" class="btn btn-default btn-sm">
</form>

<?php if ($frete > 0): ?>

<form method="post" style="float: right;" action="<?php echo BASE_URL;?>cart/payment_redirect">
    <label>Selecione o gateway(temporário):</label><br>
    <select name="payment_type">
        <option value="checkout_transparente">PagSeguro</option>
        <option value="mp">MercadoPago</option>
    </select><br><br>
    <input type="submit" value="Finalizar compra" class="btn btn-success btn-lg">
</form>

<?php endif; ?>
