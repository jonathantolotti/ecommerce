<form method="post" action="<?php echo BASE_URL; ?>users/create">
    <div class="row">

        <div class="col-sm-6">
            <div class="form-group">
                <h3>Dados Pessoais</h3>

                <label>Nome:</label>
                <input type="text" name="name" class="form-control" required/>

                <label>CPF:</label>
                <input type="text" name="cpf" class="form-control" required/>

                <label>Telefone:</label>
                <input type="text" name="telefone" class="form-control" required/>

                <label>E-mail:</label>
                <input type="email" name="email" class="form-control" required/>

                <label>Senha:</label>
                <input type="password" name="pass" class="form-control" required/>
            </div>
        </div>
        <div class="col-sm-6">
            <h3>Informações de Endereço</h3>
            <div class="form-group">

                <label>CEP:</label>
                <input type="text" name="cep" class="form-control" required/>

                <label>Rua:</label>
                <input type="text" name="rua" class="form-control" required/>

                <label>Número:</label>
                <input type="text" name="numero" class="form-control" required/>

                <label>Complemento:</label>
                <input type="text" class="form-control" name="complemento" />

                <label>Bairro:</label>
                <input type="text" name="bairro" class="form-control" required/>

                <label>Cidade:</label>
                <input type="text" name="cidade" class="form-control" required/>

                <label>Estado:</label>
                <input type="text" name="estado" class="form-control"  required/><br>
                <input type="submit" class="btn btn-success btn-lg" value="Registrar">
            </div>
        </div>

    </div>
</form>

