<?php if (isset($_SESSION['userID'])): ?>
<form method="post">
    <div class="row">

            <div class="col-sm-6">
                <div class="form-group">
                    <h3>Dados Pessoais</h3>

                    <label>Nome:</label>
                    <input type="text" name="name" class="form-control" value="Jonathan Tolotti" required/>

                    <label>CPF:</label>
                    <input type="text" name="cpf" class="form-control" value="02372079077" required/>

                    <label>Telefone:</label>
                    <input type="text" name="telefone" class="form-control" value="51987654432" required/>

                    <label>E-mail:</label>
                    <input type="email" name="email" class="form-control"
                           value="c77795441657780601711@sandbox.pagseguro.com.br" required/>

                    <label>Senha:</label>
                    <input type="password" name="password" class="form-control" value="85Dr7j88RL8JJdKc" required/>
                </div>
            </div>
            <div class="col-sm-6">
                <h3>Informações de Endereço</h3>
                <div class="form-group">

                    <label>CEP:</label>
                    <input type="text" name="cep" class="form-control" value="91920540" required/>

                    <label>Rua:</label>
                    <input type="text" name="rua" class="form-control" value="Rua Vigário Calixto" required/>

                    <label>Número:</label>
                    <input type="text" name="numero" class="form-control" value="1400" required/>

                    <label>Complemento:</label>
                    <input type="text" class="form-control" name="complemento" />

                    <label>Bairro:</label>
                    <input type="text" name="bairro" class="form-control" value="Catolé" required/>

                    <label>Cidade:</label>
                    <input type="text" name="cidade" class="form-control" value="Campina Grande" required/>

                    <label>Estado:</label>
                    <input type="text" name="estado" class="form-control" value="PB" required/>
                </div>
                <input type="submit" class="button efetuarCompra btn btn-success btn-lg
                btn-block" value="Efetuar compra">
            </div>
    </div>

</form>
<?php endif; ?>
