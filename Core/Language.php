<?php

namespace Core;

class Language
{
    private $language;
    private $dictionary;

    /**
     *
     * Language constructor.
     *
     */
    public function __construct()
    {
        global $config;
        $this->language = $config['default_lang'];

        if(!empty($_SESSION['lang']) && file_exists('Lang/' . $_SESSION['lang'] . '.ini')) {
            $this->language = $_SESSION['lang'];
        }

        $this->dictionary = parse_ini_file('Lang/' . $this->language . '.ini');
    }

    /**
     *
     * @param      $word
     * @param bool $return
     *
     * @return boolean
     *
     */
    public function get($word, $return = false)
    {
        $text = $word;

        if(isset($this->dictionary[$word])) {
            $text = $this->dictionary[$word];
        }

        if($return) {
            return $text;
        } else {
            echo $text;
        }
    }
}