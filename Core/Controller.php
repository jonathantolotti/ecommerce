<?php

namespace Core;

class Controller
{

	protected $db;
	protected $lang;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
		global $config;
		$this->lang = new Language();
	}

    /**
     * @param       $viewName
     * @param array $viewData
     */
    public function loadView($viewName, $viewData = [])
    {
		extract($viewData);
		include 'Views/'.$viewName.'.php';
	}

    /**
     * @param       $viewName
     * @param array $viewData
     */
    public function loadTemplate($viewName, $viewData = [])
    {
		include 'Views/template.php';
	}

    /**
     * @param $viewName
     * @param $viewData
     */
    public function loadViewInTemplate($viewName, $viewData)
    {
		extract($viewData);
		include 'Views/'.$viewName.'.php';
	}

}