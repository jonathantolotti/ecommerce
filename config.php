<?php
$config = [];

$environment = $_SERVER['REMOTE_ADDR'];

if ($environment == '192.168.0.17'){
    define("BASE_URL", "http://192.168.0.17/ecommerce/");
    $config['dbname']  = 'jonatha4_ecommerce';
    $config['host']    = 'jonathantolotti.com.br';
    $config['dbuser']  = 'jonatha4_root';
    $config['dbpass']  = 'camille1311';
    $config['charset'] = 'utf8';

}else{
    define("BASE_URL", "https://loja.jonathantolotti.com.br/");
    $config['dbname']  = 'jonatha4_ecommerce';
    $config['host']    = 'jonathantolotti.com.br';
    $config['dbuser']  = 'jonatha4_root';
    $config['dbpass']  = 'camille1311';
    $config['charset'] = 'utf8';
}

$config['default_lang'] = 'pt-br';
$config['cep'] = '90820030';

global $db;
try{
    $dsn = "mysql:dbname=".$config['dbname'].";"."host=".$config['host'].";"."charset=".$config['charset'].";";
    $db = new PDO($dsn, $config['dbuser'], $config['dbpass']);

}catch(PDOException $e){
    echo "ERRO:".$e->getMessage();
    exit;
}

/*
 * Configurações do PagSeguro
 */
$environment = "sandbox";
$tokenIntegrationSandBox = "334F3EFBE8194C4C85CD9A8630D5DC0A";
$config['pagseguro_seller'] = "jonathanstolotti@gmail.com";
$mailIntegrationSandBox = "jonathanstolotti@gmail.com";
$nameShop = "Nova Loja";
$release = "1.0.0";
$charset = "UTF-8";

\PagSeguro\Library::initialize();
\PagSeguro\Library::cmsVersion()->setName($nameShop)->setRelease($release);
\PagSeguro\Library::moduleVersion()->setName($nameShop)->setRelease($release);
\PagSeguro\Configuration\Configure::setEnvironment($environment);
\PagSeguro\Configuration\Configure::setAccountCredentials($mailIntegrationSandBox,$tokenIntegrationSandBox);
\PagSeguro\Configuration\Configure::setCharset($charset);
\PagSeguro\Configuration\Configure::setLog(true, 'pagseguro.log');