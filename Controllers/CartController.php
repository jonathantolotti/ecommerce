<?php

namespace Controllers;

use \Core\Controller;
use \Models\Store;
use \Models\Products;
use \Models\Cart;

class CartController extends Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $store = new Store();
        $products = new Products();
        $cart = new Cart();
        $cep = '';
        $shipping = [];

        if (!empty($_POST['cep'])) {
            $cep = intval(addslashes($_POST['cep']));

            $shipping = $cart->shippingCalculate($cep);
            $_SESSION['shipping'] = $shipping;
        }

        if (!empty($_SESSION['shipping'])) {
            $shipping = $_SESSION['shipping'];
        }

        //unset($_SESSION['cart']);

        if(!isset($_SESSION['cart']) || empty($_SESSION['cart']) ||(isset($_SESSION['cart']) && count
                ($_SESSION['cart']) == 0)) {

            $data['emptyCart'] = "Seu carrinho de compras está vazio!";
        }
        $data = $store->getTemplateData();
        $data['list'] = $cart->getList();
        $data['shipping'] = $shipping;
        $this->loadTemplate('cart', $data);
    }

    public function addToCart()
    {


        if (!empty($_POST['id_product']) &&
            !empty($_POST['qt_product']) &&
            $_POST['qt_product'] >= 1) {

            $id = intval(filter_var(addslashes($_POST['id_product']), FILTER_SANITIZE_NUMBER_INT));
            $qt = intval(filter_var(addslashes($_POST['qt_product']), FILTER_SANITIZE_NUMBER_INT));

            if (!isset($_SESSION['cart'])) {
                $_SESSION['cart'] = [];
            }

            if (isset($_SESSION['cart'][$id])) {
                $_SESSION['cart'][$id] += $qt;
            } else {
                $_SESSION['cart'][$id] = $qt;
            }

        }

        header("Location: ".BASE_URL."cart");
    }

    public function del($id)
    {
        if (!empty($id)) {
            unset($_SESSION['cart'][$id]);
        }

       header("Location: ".BASE_URL."cart");
    }

    public function payment_redirect()
    {
        if (!empty($_POST['payment_type'])) {
            $payment_type = filter_var(addslashes($_POST['payment_type']), FILTER_SANITIZE_STRING);

            switch ($payment_type) {
                case 'checkout_transparente':
                    //redireciona para o controller psCktTransparente
                    header("Location: ".BASE_URL."psCktTransparente");
                    exit;
                    break;

                case 'mp':
                    header("Location: ".BASE_URL."Mp");
                    exit;
                    break;
            }

        }
        header("Location: ".BASE_URL.'cart');
        exit;

    }

}