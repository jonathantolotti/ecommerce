<?php
namespace Controllers;

use \Core\Controller;
use Models\Cart;
use Models\Purchases;
use \Models\Store;
use \Models\Products;
use \Models\Users;

class PsCktTransparenteController extends Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $store = new Store();
        $cart = new Cart();
        $products = new Products();

        $data = $store->getTemplateData();


//inicia sessão do pagseguro
        try {
            $sessionCode = \PagSeguro\Services\Session::create(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );

            $data['sessionCode'] = $sessionCode->getResult();
        }catch(\Exception $e) {
            echo "Erro: ".$e->getMessage();
            exit;
        }

        $list = $cart->getList();
        $total = 0;

        foreach($list as $item) {
            $total += (floatval($item['price']) * intval($item['qt']));
        }

        if(!empty($_SESSION['shipping'])) {
            $shipping = $_SESSION['shipping'];

            if(isset($shipping['price'])) {
                $frete = floatval(str_replace(',', '.', $shipping['price']));
            } else {
                $frete = 0;
            }

            $total += $frete;
        }
        $total = number_format($total, 2, ".", "");
        $data['total'] = $total;

        $this->loadTemplate('cart_psCktTransparente', $data);
    }

    public function checkout()
    {
        $user = new Users();
        $cart = new Cart();
        $purchases = new Purchases();
        $uid = $_SESSION['userID'];

        $id = addslashes($_POST['id']);
        $name = addslashes($_POST['name']);
        $cpf = addslashes($_POST['cpf']);
        $telefone = addslashes($_POST['telefone']);
        $email = addslashes($_POST['email']);
        $pass = addslashes(md5($_POST['pass']));
        $cep = addslashes($_POST['cep']);
        $rua = addslashes($_POST['rua']);
        $numero = addslashes($_POST['numero']);
        $complemento = addslashes($_POST['complemento']);
        $bairro = addslashes($_POST['bairro']);
        $cidade = addslashes($_POST['cidade']);
        $estado = addslashes($_POST['estado']);
        $cartao_titular = addslashes($_POST['cartao_titular']);
        $cartao_cpf = addslashes($_POST['cartao_cpf']);
        $cartao_numero = addslashes($_POST['cartao_numero']);
        $cvv = addslashes($_POST['cvv']);
        $v_mes = addslashes($_POST['v_mes']);
        $v_ano = addslashes($_POST['v_ano']);
        $cartao_token = addslashes($_POST['cartao_token']);
        $parc = explode(';', $_POST['parc']);

        $list = $cart->getList();
        $total = 0;

        foreach($list as $item) {
            $total += (floatval($item['price']) * intval($item['qt']));
        }

        if(!empty($_SESSION['shipping'])) {
            $shipping = $_SESSION['shipping'];

            if(isset($shipping['price'])) {
                $frete = floatval(str_replace(',', '.', $shipping['price']));
            } else {
                $frete = 0;
            }

            $total += $frete;
        }
        $total = number_format($total, 2, ".", "");
        $data['total'] = $total;

        $id_purchase = $purchases->createPurchase($uid, $total, 'PAGSEGURO');
        foreach ($list as $item) {
            $purchases->addItem($id_purchase, $item['id'], $item['qt'], floatval($item['price']));
        }

        global $config;
        $creditCard = new \PagSeguro\Domains\Requests\DirectPayment\CreditCard();
        $creditCard->setReceiverEmail($config['pagseguro_seller']);
        $creditCard->setReference($id_purchase);
        $creditCard->setCurrency("BRL");

        foreach ($list as $item) {
            $creditCard->addItems()->withParameters(
                $item['id'],
                $item['name'],
                intval($item['qt']),
                floatval($item['price'])
            );
        }

        //Enviando dados para o pagseguro
        $creditCard->setSender()->setName($name);
        $creditCard->setSender()->setEmail($email);
        $creditCard->setSender()->setDocument()->withParameters('CPF', $cpf);
        $ddd = substr($telefone, 0, 2);
        $telefone = substr($telefone, 2);
        $creditCard->setSender()->setPhone()->withParameters(
            $ddd,
            $telefone
        );
        $creditCard->setSender()->setHash($id);
        $creditCard->setSender()->setIp($_SERVER['REMOTE_ADDR']);

        $creditCard->setShipping()->setAddress()->withParameters(
            $rua,
            $numero,
            $bairro,
            $cep,
            $cidade,
            $estado,
            'BRA',
            $complemento
        );
        $creditCard->setBilling()->setAddress()->withParameters(
            $rua,
            $numero,
            $bairro,
            $cep,
            $cidade,
            $estado,
            'BRA',
            $complemento
        );

        $creditCard->setToken($cartao_token);
        $creditCard->setInstallment()->withParameters($parc[0], $parc[1]);
        $creditCard->setShipping()->setCost()->withParameters($frete);
        $creditCard->setHolder()->setName($cartao_titular);
        $creditCard->setHolder()->setDocument()->withParameters('CPF', $cartao_cpf);

        $creditCard->setMode('DEFAULT');

        //url de callback
        $creditCard->setNotificationUrl(BASE_URL."PsCktTransparente/callback");

        try{
            $result = $creditCard->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            echo json_encode($result);
            exit;
        }catch(\Exception $e){
            echo json_encode([
                'error'=>true,
                'msg'=>$e->getMessage()
            ]);
        }

    }

    public function callback()
    {
        $purchase = new Purchases();
        try {
            if(\PagSeguro\Helpers\Xhr::hasPost()) {
                $r = \PagSeguro\Services\Transactions\Notification::check(
                    \PagSeguro\Configuration\Configure::getAccountCredentials()
                );

                $reference = $r->getReference();
                $status = $r->getStatus();
                /*
                 * Status pagseguro
                 * 1 - Aguardando pagamento
                 * 2 - Em análise
                 * 3 - Aprovado
                 * 4 - Disponível
                 * 5 - Em disputa
                 * 6 - Devolvida
                 * 7 - Cancelada
                 * 8 - Debitado
                 * 9 - Chargeback (Fraude)
                 */

                /*
                 * Status interno
                 * 1 - Aprovado
                 * 2 - Cancelado
                 * 3 - Disputa
                 * 4 - Chargeback
                 * 5 - Pendente
                 */

                $purchase->updateStatusTransaction($reference, $status);

            }
    }catch (\Exception $e) {
        echo "Erro: ".$e->getMessage();
    }


    }

}