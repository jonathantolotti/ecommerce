<?php
namespace Controllers;

use \Core\Controller;
use \Models\Store;
use \Models\Products;
use \Models\Categories;
use \Models\Filters;

class SearchController extends Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $store = new Store();
        $products = new Products();
        $categories = new Categories();
        $filter = new Filters();

        $data = $store->getTemplateData();

        if (!empty($_GET['product'])) {
            $searchTerm = $_GET['product'];
            $category = $_GET['category'];
            $currentFilters = [];

            if (!empty($_GET['filter']) && is_array($_GET['filter'])) {
                $currentFilters = $_GET['filter'];
            }

            $currentFilters['searchTerm'] = $searchTerm;
            $currentFilters['category'] = $category;

            $currentPage = 1;
            $limit = 6;
            $offset = 0;

            if (!empty($_GET['p'])) {
                $currentPage = $_GET['p'];
            }

            $offset = ($currentPage * $limit) - $limit;

            $data['productList'] = $products->getList($offset, $limit, $currentFilters);
            $data['totalProducts'] = $products->getTotalProducts($currentFilters);
            $data['numberPages'] = ceil(($data['totalProducts'] / $limit));
            $data['currentPage'] = $currentPage;
            $data['categories'] = $categories->getList();
            $data['filters'] = $filter->getFilters($currentFilters);
            $data['filtersSelected'] = $currentFilters;
            $data['searchTerm'] = $searchTerm;
            $data['category'] = $category;
            $data['sidebar'] = true;

            $this->loadTemplate('search', $data);
        } else {
            header("Location: ".BASE_URL);
        }

    }

}