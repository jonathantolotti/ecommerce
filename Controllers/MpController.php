<?php
namespace Controllers;

use Core\Controller;
use Models\Cart;
use Models\Products;
use Models\Purchases;
use Models\Store;
use MP;

class MpController extends Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $store = new Store();
        $cart = new Cart();
        $products = new Products();
        $purchases = new Purchases();
        $uid = $_SESSION['userID'];

        $data = $store->getTemplateData();

        if (!empty($_POST['name'])) {
            $name = addslashes($_POST['name']);
            $cpf = addslashes($_POST['cpf']);
            $telefone = addslashes($_POST['telefone']);
            $email = addslashes($_POST['email']);
            $pass = addslashes(md5($_POST['password']));
            $cep = addslashes($_POST['cep']);
            $rua = addslashes($_POST['rua']);
            $numero = addslashes($_POST['numero']);
            $complemento = addslashes($_POST['complemento']);
            $bairro = addslashes($_POST['bairro']);
            $cidade = addslashes($_POST['cidade']);
            $estado = addslashes($_POST['estado']);

            $list = $cart->getList();
            $frete = 0;
            $total = 0;

            $urlReturn = 'https://loja.jonathantolotti.com.br/users/myorders';
            $urlCallback = 'https://loja.jonathantolotti.com.br/Mp/callback';
            $urlNotification = 'https://loja.jonathantolotti.com.br/mp/notification';

            foreach($list as $item) {
                $total += (floatval($item['price']) * intval($item['qt']));
            }

            if(!empty($_SESSION['shipping'])) {
                $shipping = $_SESSION['shipping'];

                if(isset($shipping['price'])) {
                    $frete = floatval(str_replace(',', '.', $shipping['price']));
                } else {
                    $frete = 0;
                }
                $total += $frete;
            }
            $id_purchase = $purchases->createPurchase($uid, $total, 'MERCADOPAGO');
            foreach ($list as $item) {
                $purchases->addItem($id_purchase, $item['id'], $item['qt'], floatval($item['price']));
            }

            try {
                $mp = new MP('4143033617310476', 'MnzG0L1zLgSl5RyCk8SnDSVe9rN6vVra');
            } catch(\MercadoPagoException $e) {
                echo "Erro: ".$e->getMessage();
            }

            $dataMp = [
                'items' => [],
                'shipments' => [
                    'mode' => 'custom',
                    'cost' => $frete,
                    'receiver_address' => [
                        'zip_code' => $cep
                    ]
                ],
                'back_urls' => [
                    'success' => $urlReturn,
                    'pending' => $urlReturn,
                    'failure' => $urlReturn
                ],
                'notification_url' => $urlNotification,
                'auto_return' => 'all',
                'external_reference' => $id_purchase
            ];

            foreach ($list as $item) {
                $dataMp['items'][] = [
                    'title' => $item['name'],
                    'quantity' => $item['qt'],
                    'currency_id' => 'BRL',
                    'unit_price' => floatval($item['price'])
                ];
            }

            $link = $mp->create_preference($dataMp);

            if ($link['status'] == '201') {
                $link = $link['response']['init_point'];//Link de produção
                //$link = $link['response']['sandbox_init_point']; //Link sandbox
                header("Location: ".$link);
                exit;
            } else {
                $data['error'] = 'Tente novamente mais tarde.';
            }


        }

        $this->loadTemplate('cart_mp', $data);
    }

    public function notification()
    {
        $purchase = new Purchases();
        try {
            $mp = new MP('4143033617310476', 'MnzG0L1zLgSl5RyCk8SnDSVe9rN6vVra');
        } catch(\MercadoPagoException $e) {
            echo "Erro: ".$e->getMessage();
        }

        $mp->sandbox_mode(false);

        $info = $mp->get_payment_info($_GET['id']);

        if ($info['status'] == '200') {
            $array = $info['response'];
            $reference = $array['collection']['external_reference'];
            $status = $array['collection']['status'];
            /*
             * pending - Em analise
             * approved - Aprovado
             * in_process = Em analise
             * in_mediation = Disputa
             * rejected - Rejeitado
             * canceled - Cancelado
             * refunded - Reembolsado
             * chaged_back - Chargeback
             */

            if ($status == 'pending') {
                $purchase->updateStatusTransaction($reference, 1);
            } elseif ($status == 'approved') {
                $purchase->updateStatusTransaction($reference, 3);
            } elseif ($status == 'in_process') {
                $purchase->updateStatusTransaction($reference, 2);
            } elseif ($status == 'in_mediation') {
                $purchase->updateStatusTransaction($reference, 5);
            } elseif ($status == 'rejected') {
                $purchase->updateStatusTransaction($reference, 6);
            } elseif ($status == 'cancelled') {
                $purchase->updateStatusTransaction($reference, 5);
            } elseif ($status == 'refunded') {
                $purchase->updateStatusTransaction($reference, 6);
            } elseif ($status == 'charged_back') {
                $purchase->updateStatusTransaction($reference, 9);
            }
            file_put_contents('mplog.txt', print_r($array, true));
        }
    }
}