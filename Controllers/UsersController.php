<?php
namespace Controllers;

use \Core\Controller;
use \Models\Users;
use \Models\Store;


class UsersController extends Controller
{
    public function index()
    {

    }
    public function validate()
    {
        $data = [];
        $store = new Store();
        $user = new Users();
        $data = $store->getTemplateData();
        if (isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['pass']) && !empty
            ($_POST['pass'])) {

            $email = filter_var(addslashes($_POST['email']), FILTER_SANITIZE_EMAIL);
            $pass = md5(filter_var(addslashes($_POST['pass']), FILTER_SANITIZE_STRING));

            if ($user->validate($email, $pass)) {
                header("Location: ".BASE_URL.'home');
            }



        }
        $this->loadTemplate('userLogin', $data);
    }

    public function create()
    {
        $store = new Store();
        $user = new Users();
        $data = [];
        $data = $store->getTemplateData();
        if (isset($_POST['name'])){
            $name = addslashes($_POST['name']);
            $cpf = addslashes($_POST['cpf']);
            $telefone = addslashes($_POST['telefone']);
            $email = addslashes($_POST['email']);
            $pass = addslashes(md5($_POST['pass']));
            $cep = addslashes($_POST['cep']);
            $rua = addslashes($_POST['rua']);
            $numero = addslashes($_POST['numero']);
            $complemento = addslashes($_POST['complemento']);
            $bairro = addslashes($_POST['bairro']);
            $cidade = addslashes($_POST['cidade']);
            $estado = addslashes($_POST['estado']);


            if ($user->createUser($name, $cpf, $telefone, $email, $pass, $cep, $rua, $numero,
                $complemento, $bairro, $cidade, $estado)) {

                header("Location: ".BASE_URL.'psCktTransparente');
            }
        }

        $this->loadTemplate('createUser', $data);
    }

    public function logout()
    {
        unset($_SESSION['userID']);
        header("Location: ".BASE_URL);
    }

    public function myOrders()
    {
        $store = new Store();
        $user = new Users();
        $data = [];
        $data = $store->getTemplateData();

        $data['orders'] = $user->getMyOrders($_SESSION['userID']);
        $this->loadTemplate('myOrders', $data);

    }
}