<?php
namespace Controllers;

use \Core\Controller;
use \Models\Store;
use \Models\Filters;

class NotFoundController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $filter = new Filters();
        $store = new Store();
        $data = [];
        $data['filters'] = $filter->getFilters();
        $data = $store->getTemplateData();
        $this->loadTemplate('404', $data);
    }

}