<?php
namespace Controllers;

use \Core\Controller;
use \Models\Store;
use \Models\Products;
use \Models\Categories;

class ProductController extends Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        header("Location: ".BASE_URL);
    }

    public function open($idProduct)
    {
        $store = new Store();
        $products = new Products();
        $categories = new Categories();

        $data = $store->getTemplateData();
        $info = $products->getProductInfo($idProduct);


        if (count($info) > 0) {
            $currentFilters = [];
            $offset = 0;
            $limitRates = 5;

            $data['product_rates'] = $products->getRates($idProduct, $limitRates);
            $data['product_options'] = $products->getOptionsByProductId($idProduct);
            $data['product_info'] = $info;
            $data['product_images'] = $products->getAllImagesByProductId($idProduct);
            $data['categories'] = $categories->getList();

            $this->loadTemplate('product', $data);
        } else {
            header("Location: ".BASE_URL);
        }



    }

}