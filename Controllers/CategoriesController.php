<?php

namespace Controllers;

use \Core\Controller;
use \Models\Store;
use \Models\Products;
use \Models\Categories;
use \Models\Filters;

class CategoriesController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        header("Location: ".BASE_URL);
    }

    /**
     * @param $idCategory
     */
    public function enter($idCategory)
    {
        $store = new Store();
        $categories = new Categories();
        $products = new Products();
        $filter = new Filters();

        $data = $store->getTemplateData();
        $currentFilters = [];
        $data['categoryName'] = $categories->getCategoryName($idCategory);

        if (!empty($data['categoryName'])) {

            $currentPage = 1;
            $limit = 6;
            $offset = 0;

            if (!empty($_GET['p'])) {
                $currentPage = $_GET['p'];
            }
            $offset = ($currentPage * $limit) - $limit;

            $data['categoryFilter'] = $categories->getCategoryTree($idCategory);

            $filters = [
                'category' => $idCategory
            ];

            $data['productList'] = $products->getList($offset, $limit, $filters);
            $data['totalProducts'] = $products->getTotalProducts($filters);
            $data['numberPages'] = ceil(($data['totalProducts'] / $limit));
            $data['currentPage'] = $currentPage;
            $data['idCategory'] = $idCategory;

            $data['categories'] = $categories->getList();
            $data['filters'] = $filter->getFilters($currentFilters);
            $data['sidebar'] = true;
            $this->loadTemplate('categories', $data);
        } else {
            header('Location: '.BASE_URL);
        }

    }
}