<?php
namespace Controllers;

use \Core\Controller;
use \Models\Store;
use \Models\Products;
use \Models\Categories;
use \Models\Filters;

class HomeController extends Controller
{
	private $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $store = new Store();
        $products = new Products();
        $categories = new Categories();
        $filter = new Filters();

        $data = $store->getTemplateData();
        $currentFilters = [];

        if (!empty($_GET['product'])) {
            $searchTerm = $_GET['product'];
            $category = $_GET['category'];
        }

        if (!empty($_GET['filter']) && is_array($_GET['filter'])) {
            $currentFilters = $_GET['filter'];
        }

        $currentPage = 1;
        $limit = 6;
        $offset = 0;


        if (!empty($_GET['p'])) {
            $currentPage = $_GET['p'];
        }

        $offset = ($currentPage * $limit) - $limit;

        $data['sidebar'] = true;
        $data['productList'] = $products->getList($offset, $limit, $currentFilters);
        $data['totalProducts'] = $products->getTotalProducts($currentFilters);
        $data['numberPages'] = ceil(($data['totalProducts'] / $limit));
        $data['currentPage'] = $currentPage;


        $data['filters'] = $filter->getFilters($currentFilters);
        $data['filtersSelected'] = $currentFilters;


        $this->loadTemplate('home', $data);
    }

}