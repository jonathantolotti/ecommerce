<?php

namespace Models;

use \Core\Model;

class Store extends Model
{

    public function getTemplateData()
    {
        $data = [];
        $categories = new Categories();
        $products = new Products();
        $cart = new Cart();

        $offset = 0;
        $limitFeatured1 = 5;
        $limitFeatured2 = 5;
        $limitSale = 3;
        $limitTopRated = 3;

        if (isset($_SESSION['cart'])) {
            $qt = 0;
            foreach ($_SESSION['cart'] as $qtd) {
                $qt += intval($qtd);
            }
            $data['cart_qt'] = $qt;
        }

        $data['cart_price'] = $cart->getSubtotal();

        $data['categories'] = $categories->getList();
        $data['widget_featured1'] = $products->getList($offset, $limitFeatured1, ['featured' => '1'], true);
        $data['widget_featured2'] = $products->getList($offset, $limitFeatured2, ['featured' => '1'], true);
        $data['widget_sale'] = $products->getList($offset, $limitSale, ['sale' => '1'], true);
        $data['widget_toprated'] = $products->getList($offset, $limitTopRated, ['toprated' => '1'], true);

        return $data;
    }


}