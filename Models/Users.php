<?php
namespace Models;

use \Core\Model;

class Users extends Model
{
    public function emailExists($email)
    {
        $sql = "SELECT * FROM users WHERE email = :email";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":email", $email);
        $sql->execute();

        if($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validate($email, $pass)
    {
        $uid = '';

        $sql = "SELECT * FROM users WHERE email = :email AND password = :pass";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":email", $email);
        $sql->bindValue(":pass", $pass);
        $sql->execute();

        if($sql->rowCount() > 0) {
            $sql = $sql->fetch();
            $uid = $sql['id'];
            $_SESSION['userID'] = $uid;
        }

        return $uid;
    }

    public function createUser($name, $cpf, $telefone, $email, $pass, $cep, $rua, $numero,
                               $complemento, $bairro, $cidade, $estado)
    {

        /*

        $sql = "INSERT INTO users (email, password, name, cpf, phone, zipcode, street, number, complement,
                                  district, city, state)
                                  VALUES (:email, :password, :name, :cpf, :phone, :zipcode, 
                                  :street, :number, :complement, :district, :city, :state)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->bindValue(":cpf", $cpf);
        $sql->bindValue(":phone", $telefone);
        $sql->bindValue(":email", $email);
        $sql->bindValue(":pass", $pass);
        $sql->bindValue(":zipcode", $cep);
        $sql->bindValue(":street", $rua);
        $sql->bindValue(":number", $numero);
        $sql->bindValue(":complement", $complemento);
        $sql->bindValue(":district", $bairro);
        $sql->bindValue(":city", $cidade);
        $sql->bindValue(":state", $estado);
        $sql->execute();

        $this->validate($email, $pass);

        return true;
        */


        $sql = "INSERT INTO users SET email = '$email', password = '$pass', name = '$name', cpf = $cpf, 
                phone = $telefone, zipcode = '$cep', street = '$rua', number = $numero, complement =
                '$complemento',district = '$bairro', city = '$cidade', state = '$estado'";
        $sql = $this->db->query($sql);

        $this->validate($email, $pass);

        return true;
    }

    public function getMyOrders($idUser)
    {
        $orders = [];

        $sql = "SELECT *,
                (SELECT statusStore FROM 
                pagseguro_transaction_status WHERE 
                purchases.payment_status = pagseguro_transaction_status.idPagseguro) as status_store 
                FROM 
                purchases WHERE id_user = :id order by id desc limit 10";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $idUser);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $orders = $sql->fetchAll();
        }

        return $orders;
    }
}