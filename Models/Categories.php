<?php

namespace Models;

use \Core\Model;

class Categories extends Model
{

    /**
     * @return array
     */
    public function getList()
    {
        $array = [];

        $sql = "SELECT * FROM categories ORDER BY sub DESC";
        $sql = $this->db->query($sql);

        if($sql->rowCount() > 0) {
            foreach($sql->fetchAll() as $item) {
                $item['subs'] = array();
                $array[$item['id']] = $item;
            }

            while($this->stillNeed($array)) {
                Categories::organizeCategory($array);
            }
        }
        return $array;
    }

    /**
     * @param $idCategory
     *
     * @return array
     */
    public function getCategoryTree($idCategory)
    {
        $categoryTree = [];
        $haveChild = true;

        while($haveChild) {

            $sql = "SELECT * FROM categories WHERE id = :id";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":id", $idCategory);
            $sql->execute();
            if($sql->rowCount() > 0) {
                $sql = $sql->fetch();
                $categoryTree[] = $sql;

                if(!empty($sql['sub'])) {
                    $idCategory = $sql['sub'];
                } else {
                    $haveChild = false;
                }
            }

        }
        $categoryTree = array_reverse($categoryTree); // inverte o array de categorias
        return $categoryTree;
    }

    public function getCategoryName($idCategory)
    {
        $sql = $this->db->prepare('SELECT name FROM categories WHERE id = :id');
        $sql->bindValue(":id", $idCategory);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $sql = $sql->fetch();
            return $sql['name'];
        }
    }


    private function organizeCategory(&$array)
    {
        foreach($array as $id => $item) {
            if(isset($array[$item['sub']])) {
                $array[$item['sub']]['subs'][$item['id']] = $item;
                unset($array[$id]);
                break;
            }
        }
    }

    private function stillNeed($array)
    {
        foreach($array as $item) {
            if(!empty($item['sub'])) {
                return true;
            }
        }
        return false;
    }
}