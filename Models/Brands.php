<?php

namespace Models;

use \Core\Model;

class Brands extends Model
{
    /**
     * @return array
     */
    public function getBrandList()
    {
        $brands = [];
        $sql = $this->db->query("SELECT * FROM brands");

        if ($sql->rowCount() > 0) {
            $brands = $sql->fetchAll();
        }
        return $brands;
    }
}