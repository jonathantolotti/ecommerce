<?php

namespace Models;

use \Core\Model;

class Rates extends Model
{
    public function getRates($id, $limit)
    {
        $array = [];
        $sql = "SELECT *,
                (SELECT users.name FROM users WHERE users.id = rates.id_user) as user_name
                FROM rates WHERE id_product = :id ORDER BY date_rated DESC LIMIT ".$limit;
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array = $sql->fetchAll();
        }

        return $array;
    }
}