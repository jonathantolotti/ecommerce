<?php

namespace Models;

use \Core\Model;

class Filters extends Model
{
    /**
     * @param array $currentFilters
     *
     * @return array
     */
    public function getFilters($currentFilters = [])
    {
        $brands = new Brands();
        $products = new Products();
        $filters = [
            'searchTerm' => '',
            'brands' => '',
            'slider0' => 0,
            'slider1' => 0,
            'maxslider' => '1000',
            'minslider' => '250',
            'stars' => [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0
            ],
            'sale' => 0,
            'options' => []
        ];

        $brandProducts = $products->getListOfBrands($currentFilters);
        $filters['brands'] = $brands->getBrandList();

        //criando filtro de marcas
        foreach ($filters['brands'] as $bkey => $bitem) {
            $filters['brands'][$bkey]['count'] = '0';
            foreach ($brandProducts as $bProduct) {
                if ($bProduct['id_brand'] == $bitem['id']) {
                    $filters['brands'][$bkey]['count'] = $bProduct['total'];
                }
            }
            if ($filters['brands'][$bkey]['count'] == '0') {
                unset($filters['brands'][$bkey]);
            }
        }

        //criando filtro de preço maior
        $filters['maxslider'] = $products->getMaxPrice($currentFilters);
        $filters['minslider'] = $products->getMinPrice($currentFilters);

       if ($filters['slider0'] == 0) {
            $filters['slider0'] = $filters['minslider'];
        }

        if ($filters['slider1'] == 0) {
            $filters['slider1'] = $filters['maxslider'];
        }

        //criando filtro das estrelas
        $starProducts = $products->getListOfStar($currentFilters);
        foreach ($filters['stars'] as $starKey => $starItem) {
            foreach ($starProducts as $sproducts) {
                if ($sproducts['rating'] == $starKey) {
                    $filters['stars'][$starKey] = $sproducts['total'];
                }
            }
        }

        //criando filtro das promoções
        $filters['sale'] = $products->getSaleCount($currentFilters);

        // criando filtro das opções

        $filters['options'] = $products->getAvailableOptions($currentFilters);

        return $filters;
    }
}