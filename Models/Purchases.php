<?php
namespace Models;

use Core\Model;

class Purchases extends Model
{
    public function createPurchase($uid, $total, $payment_type)
    {
        $sql = "INSERT INTO purchases SET id_user = :uid, 
                                          total_amount = :total, 
                                          payment_type = :payment_type, 
                                          payment_status = 1";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":uid", $uid);
        $sql->bindValue(":total", $total);
        $sql->bindValue(":payment_type", $payment_type);
        $sql->execute();

        return $this->db->lastInsertId();

    }

    public function addItem($id, $id_product, $qt, $price)
    {
        $sql = "INSERT INTO purchases_products SET id_purchase = :id, 
                                                  id_product = :id_product, 
                                                  quantity = :qt, 
                                                  product_price = :price";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->bindValue(":id_product", $id_product);
        $sql->bindValue(":qt", $qt);
        $sql->bindValue(":price", $price);
        $sql->execute();
    }

    public function updateStatusTransaction($idTransaction, $idStatus)
    {
        /*
         * 1 - Aprovado
         * 2 - Cancelado
         * 3 - Disputa
         * 4 - Chargeback
         * 5 - Pendente
         */
        $sql = "UPDATE purchases SET payment_status = :status WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":status", $idStatus);
        $sql->bindValue(":id", $idTransaction);
        $sql->execute();
    }
}