<?php

namespace Models;

use \Core\Model;

class Products extends Model
{
    public function getInfo($id)
    {
        $array = [];

        $sql = "SELECT * FROM products WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array = $sql->fetch();
            $images = Products::getImagesByProductId($id);
            $array['image'] = current($images);
        }

        return $array;
    }

    /**
     * @param int   $offset
     *
     * @param int   $limit
     * @param array $filters
     *
     * @return array
     */
    public function getList($offset = 0, $limit = 0, $filters = [], $random = false)
    {
        $return = [];
        $where = Products::buildWhere($filters);
        $orderBySQL = "";

        if ($random == true) {
            $orderBySQL = "ORDER BY RAND()";
        }

        if (!empty($filters['toprated'])) {
            $orderBySQL = "ORDER BY rating DESC";
        }

        $sql = "SELECT *,
                (SELECT name FROM brands where brands.id = products.id_brand) as brandName,
                (SELECT name FROM categories WHERE categories.id = products.id_category) as categoryName
                FROM products WHERE " . implode(' AND ', $where) . "
                ".$orderBySQL."
                LIMIT $offset, $limit";
        $sql = $this->db->prepare($sql);
        Products::bindWhere($filters, $sql);
        $sql->execute();

        if($sql->rowCount() > 0) {
            $return = $sql->fetchAll();

            foreach($return as $key => $item) {
                $return[$key]['images'] = Products::getImagesByProductId($item['id']);
            }
        }
        return $return;
    }

    /**
     * @param array $filters
     *
     * @return mixed
     */
    public function getTotalProducts($filters = [])
    {
        $where = Products::buildWhere($filters);

        $sql = "SELECT COUNT(*) as total 
                FROM products
                 WHERE " . implode(' AND ', $where);
        $sql = $this->db->prepare($sql);

        Products::bindWhere($filters, $sql);

        $sql->execute();
        $sql = $sql->fetch();

        return $sql['total'];
    }

    /**
     * @param $id
     *
     * @return array|mixed
     */
    public function getImagesByProductId($id)
    {
        $return = [];
        $sql = $this->db->prepare('SELECT url FROM products_images WHERE id_product = :id');
        $sql->bindValue(":id", $id);
        $sql->execute();

        if($sql->rowCount() > 0) {
            $return = $sql->fetch();
        }

        return $return;
    }

    public function getAllImagesByProductId($id)
    {
        $return = [];
        $sql = $this->db->prepare('SELECT url FROM products_images WHERE id_product = :id');
        $sql->bindValue(":id", $id);
        $sql->execute();

        if($sql->rowCount() > 0) {
            $return = $sql->fetchAll();
        }

        return $return;
    }

    public function getOptionsByProductId($id)
    {
        $options = [];

        //pega o nome das opções
        $sql = "SELECT options FROM products WHERE id = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $options = $sql->fetch();
            $options = $options['options'];

            if (!empty($options)) {
                $sql = "SELECT * FROM options WHERE id IN (".$options.")";
                $sql = $this->db->query($sql);

                $options = $sql->fetchAll();
            }
            //pega o valor das opções
            $sql = "SELECT * FROM products_options WHERE :id_product = :id_product";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":id_product", $id);
            $sql->execute();

            $options_values = [];
            if ($sql->rowCount() > 0) {
                foreach ($sql->fetchAll() as $op) {
                    $options_values[$op['id_option']] = $op['p_value'];
                }
            }

            //junta todos resultados em um array
            foreach ($options as $ok => $op) {
                if (isset($options_values[$op['id']])) {
                    $options[$ok]['values'] = $options_values[$op['id']];
                } else {
                    $options[$ok['value']] = '';
                }
            }
        }

        return $options;
    }

    public function getRates($id, $limit)
    {
        $array = [];
        $rates = new Rates();

        $array = $rates->getRates($id, $limit);
        return $array;
    }

    /**
     * @param array $filters
     *
     * @return array
     */
    public function getListOfBrands($filters = [])
    {
        $brandProduct = [];
        $where = Products::buildWhere($filters);
        $sql = $this->db->prepare("SELECT id_brand, COUNT(id_brand) as total 
                                 FROM products 
                                 WHERE " . implode(' AND ', $where) . "
                                 GROUP BY id_brand");
        Products::bindWhere($filters, $sql);
        $sql->execute();

        if($sql->rowCount() > 0) {
            $brandProduct = $sql->fetchAll();
        }

        return $brandProduct;
    }

    public function getListOfStar($filters = [])
    {
        $rating = [];
        $where = Products::buildWhere($filters);
        $sql = $this->db->prepare("SELECT rating, COUNT(id_brand) as total 
                                 FROM products 
                                 WHERE " . implode(' AND ', $where) . "
                                 GROUP BY rating");
        Products::bindWhere($filters, $sql);
        $sql->execute();

        if($sql->rowCount() > 0) {
            $rating = $sql->fetchAll();
        }

        return $rating;
    }

    public function getSaleCount($filters = [])
    {
        $where = Products::buildWhere($filters);
        $where[] = 'sale = "1"';

        $sql = "SELECT COUNT(*) as total 
                FROM products 
                WHERE " . implode(' AND ', $where);
        $sql = $this->db->prepare($sql);
        Products::bindWhere($filters, $sql);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $sql = $sql->fetch();
            return $sql['total'];
        } else {
            return '0';
        }
    }

    public function getMaxPrice($filters = [])
    {
        $sql = "SELECT price 
                FROM products 
                ORDER BY price DESC LIMIT 1";
        $sql = $this->db->prepare($sql);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $sql = $sql->fetch();
            return $sql['price'];
        } else {
            return '0';
        }
    }

    public function getMinPrice($filters = [])
    {
        $where = Products::buildWhere($filters);

        $sql = "SELECT price 
                FROM products 
                WHERE " . implode(' AND ', $where) . "
                ORDER BY price ASC LIMIT 1";
        $sql = $this->db->prepare($sql);
        Products::bindWhere($filters, $sql);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $sql = $sql->fetch();
            return $sql['price'];
        } else {
            return '0';
        }
    }

    public function getAvailableOptions($filters = [])
    {
        $groups = [];
        $ids = [];

        $where = Products::buildWhere($filters);

        $sql = "SELECT id, options
                FROM products
                 WHERE " . implode(' AND ', $where);
        $sql = $this->db->prepare($sql);
        Products::bindWhere($filters, $sql);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            foreach ($sql->fetchAll() as $product) {
                $options = explode(",", $product['options']);
                $ids[] = $product['id'];

                foreach ($options as $op) {
                    if (!in_array($op, $groups)) {
                        $groups[] = $op;
                    }
                }
            }
        }
        $options = Products::getAvailableValuesFromOptions($groups, $ids);
        return $options;
    }

    /**
     * @param $groups
     * @param $ids
     *
     * @return array
     */
    private function getAvailableValuesFromOptions($groups, $ids)
    {
        $options = new Options();
        $return = [];

        foreach ($groups as $op) {
            $return[$op] = [
                'name' => $options->getOptionName($op),
                'options' => []
            ];
        }

        $sql = "SELECT p_value, id_option, COUNT(*) as total
                FROM products_options
                WHERE
                id_option IN ('".implode("','", $groups)."') AND
		        id_product IN ('".implode("','", $ids)."')
                GROUP BY p_value ORDER BY id_option";

        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            foreach ($sql->fetchAll() as $ops) {
                $return[$ops['id_option']]['options'][] = [
                    'id' => $ops['id_option'],
                    'value' => $ops['p_value'],
                    'count' => $ops['total']
                ];
            }
        }

        return $return;
    }

    public function getProductInfo($idProduct)
    {
        $array = [];

        if (!empty($idProduct)) {
            $sql = "SELECT *,
                   (SELECT name FROM brands where brands.id = products.id_brand) as brandName
                    FROM products WHERE id = :id";
            $sql = $this->db->prepare($sql);
            $sql->bindValue(":id", $idProduct);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                $array = $sql->fetch();
            }
        }
        return $array;
    }
    /**
     * @param $filters
     *
     * @return array
     */
    private function buildWhere($filters)
    {
        $where = array(
            '1=1'
        );

        if(!empty($filters['category'])) {
            $where[] = "id_category = :id_category";
        }

        if(!empty($filters['brand'])) {
            $where[] = "id_brand IN ('".implode("','", $filters['brand'])."')";
        }

        if(!empty($filters['star'])) {
            $where[] = "rating IN ('".implode("','", $filters['star'])."')";
        }

        if(!empty($filters['sale'])) {
            $where[] = "sale = '1'";
        }

        if(!empty($filters['featured'])) {
            $where[] = "featured = '1'";
        }

        if(!empty($filters['toprated'])) {
            $where[] = "rating > '0'";
        }

        if(!empty($filters['options'])) {
            $where[] = "id IN (select id_product from products_options where products_options.p_value IN 
            ('".implode("','", $filters['options'])."'))";
        }

        if(!empty($filters['slider0'])) {
            $where[] = "price >= :slider0";
        }

        if(!empty($filters['slider1'])) {
            $where[] = "price <= :slider1";
        }

        if(!empty($filters['searchTerm'])) {
            $where[] = "name LIKE :searchTerm";
        }

        return $where;
    }

    /**
     * @param $filters
     * @param $sql
     */
    private function bindWhere($filters, &$sql)
    {
        if(!empty($filters['category'])) {
            $sql->bindValue(":id_category", $filters['category']);
        }

        if(!empty($filters['slider0'])) {
            $sql->bindValue(":slider0", $filters['slider0']);
        }

        if(!empty($filters['slider1'])) {
            $sql->bindValue(":slider1", $filters['slider1']);
        }

        if(!empty($filters['searchTerm'])) {
           $sql->bindValue(":searchTerm", '%'.$filters['searchTerm'].'%');
        }


    }
}